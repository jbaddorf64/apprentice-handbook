## Vacation and Days Off

If you would like to request time off, there is an option to do so through the When I Work Dashboard. Please submit time off requests at least 2 weeks in advance so the leadership team has ample time to review and plan ahead for your absence. MAXX Potential is flexible in approving time off as long as doing so does not interfere with company operations. 

Vacation and personal time off days, are granted on an unpaid basis to hourly apprentice employees.

## Sick Leave

Situations may arise where an apprentice employee needs to take time off to address medical or other health concerns. MAXX Potential requests that apprentice employees provide a notification email to their advisor as soon as practicable when having to take time off that has not been preapproved, as we may need to re-distribute your work if necessary. Time taken for health concerns is granted on an unpaid basis. 

## Jury Duty Time Off

MAXX Potential understands that occasionally apprentice employees are called to serve on a jury. Apprentice employees who are selected for jury duty must provide a copy of their jury summons to their advisor. Time taken for jury duty is granted on an unpaid basis. 

## Voting Time Off

Apprentice employees are encouraged to participate in elections. MAXX Potential grants incremental time off to cast a ballot in an election. Voting time off is granted on an unpaid basis. Should extenuating circumstances arise while voting, notify your advisor as soon as possible.

## Military Leave

Employees called to active military duty, military reserve or National Guard service may be eligible to receive time off under the Uniformed Services Employment and Reemployment Rights Act of 1994. To receive time off, apprentice employees must provide notice and a copy of their report orders to an immediate advisor. Military leave is granted on an unpaid basis. Upon return with an honorable discharge, an apprentice employee may be entitled to reinstatement and any applicable job benefits they would have received if present, to the extent provided by law.

## Bereavement Leave

MAXX Potential is sympathetic to the needs of employees and the families of employees when there is loss of a loved one. MAXX provides Bereavement Leave to allow employees to attend to the legal, financial, and emotional needs of their families in the event of such loss. Employees taking Bereavement Leave will notify their advisor as soon as possible of the funeral arrangements, anticipated length of leave, where the employee can be reached during the Leave, and other appropriate information. In no case will Bereavement Leave begin before the Supervisor is notified. Time taken for Bereavement Leave is granted on an unpaid basis. 

## Leave of Absence

Regular full-time employees may request an unpaid leave of absence. A request for a leave of absence must be submitted in writing in advance to the apprentice employee's immediate advisor. Leaves of absences that are granted are unpaid.

## Other Leaves not Mentioned

MAXX will grant leaves of absence for other activities as required by law.  Unless otherwise required by law, employees will not be paid for such leaves of absence.  
