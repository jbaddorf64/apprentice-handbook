# Wage and Compensation

## Pay Practices

Fair compensation is critical to making MAXX a desirable place to work and helping our employees lead happy, healthy lives inside and outside of the office. Our pay structure is designed to meet several goals. We seek to:

* Provide pay that reflects the value your work provides to the company.
* Ensure that there is transperency within the company, and fairness across the company.
* Make decisions about pay based on skills and value without regard to race, color, religion, age, national origin, marital status, pregnancy, disability, sexual orientation, gender, sex, gender identity or expression, genetic information, military status, or any other basis protected by applicable law.

### Payment Structure

Level| Hourly Rate
-----|-------------
  1  |   10.50
  2  |   12.00
  3  |   13.50
  4  |   15.00

### Payday Procedures

The MAXX work-week runs from Sunday to Saturday. Each pay period consists of two work-weeks and is paid out one week folowing. This means that each paycheck you receive reflects the hours worked for the two-week period ending the previous Saturday. Payday is every other Friday for all employees. 

**You are responsible** for making sure your pay is **correct.** It is your responsibility to make sure that your hours are correct by the Monday before each payday. Please review your tax witholding and pay stub each pay period, particularly, the rate of pay, hours worked and deductions, to ensure that they are true and accurate. If your check is incorrect, please notify the Payroll Administrator at once so that we can make the needed adjustment.

### Work Schedules

Employees should set up their own work schedules through the company's time and attendance system. It is imperative that employees show up when they say they are available, so that the Leadership Team may plan projects and work around who will be in the building. Any major changes in availabilty should be discussed with your advisor with at least two weeks notice.

### Time Clock

Employees are to clock in and out from the time clock terminal. If the terminal is down due to connectivity issues, please contact the Payroll Administrator with an email of your in/out time immediately. Any abuse of the time clock system is subject to further disciplinary action, up to and including termination.


### Overtime Pay

Overtime is considered any amount of hours over 40 in a single workweek. You must have explicit permission from your advisor to work over 40 hours in a week, and will only be granted permission if there is a business justification for you to do so. If approved to work overtime, you will be paid one and one-half times your usual pay rate for all hours worked over 40 in a workweek.
