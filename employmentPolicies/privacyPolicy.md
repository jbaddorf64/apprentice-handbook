# Employee Privacy

## Workspace Privacy

As an employer, we want employees to feel a sense of ownership towards their personal workspaces and be comfortable at the office. However, in order to ensure the safety and well-being of MAXX employees and the security of our customer's data, company property (e.g. desks, laptops, and other devices) is subject to search.

MAXX Potential reserves the right to search company property at any time, without warning, to ensure compliance with our policies, including those that cover employee safety, workplace violence, harassment, theft, and possession of prohibited items. If you use a lock on any item that will be used to store company property, you must give a copy of the key or combination to your advisor.

## Email and Internet Privacy

MAXX Potential email and Internet use are company resources that are managed according to company policy, and therefore subject to search and/or monitoring. The company has both the ability and the right to look at employee usage as needed to protect employee safety and wellbeing, as well as the security of customer data, company property and interests.

## Email Is Not Private

Email messages, including attachments, sent and received from a MAXX Potential email address are the property of MAXX Potential. We reserve the right to access, monitor, read, and/or copy email messages at any time, for any reason. You should not expect privacy for any email you send using your MAXX Potential email, including messages that you consider to be personal or label with a designation such as “Personal” or “Private.”

## Use of the Email System for Personal Email

The email system is intended for official company business. Sending personal messages using the MAXX Potential email system is strongly discouraged.

## All Conduct Rules Apply to Email

All of our policies and rules of conduct apply to employee use of the email system. This means, for example, that you may not use the email system to send harassing or discriminatory messages, including messages with explicit sexual content or pornographic images; to send threatening messages; or to reveal company trade secrets.

## Professional Tone and Content

We expect you to exercise discretion in using electronic communications equipment. When you send email using a MAXX Potential email address or MAXX Potential social account, you are representing MAXX Potential. Make sure that your messages are professional and appropriate, in tone and content. Remember, although email or social media may seem like a private conversation, they can be printed, saved, and forwarded to unintended recipients. You should not send any email/message/update/etc. that you wouldn’t want our team, your family, or our competitors to read.

## Email Security

To avoid email viruses, phishing for account information, and other threats, employees should not open email attachments from people and businesses they don’t recognize, particularly if the email appears to have been forwarded multiple times or has a nonexistent or peculiar subject heading. Even if you know the sender, do not open an email attachment that has a strange name or is not referenced in the body of the email; it may have been transmitted automatically, without the sender’s knowledge.

If you believe your computer has been infected by a virus, worm, or other security threat, you must inform the leadership team immediately.

Employees may not share their email passwords with anyone, including coworkers or family members. Revealing passwords to MAXX Potential's email system could allow an outsider to attack the MAXX Potential network.

## Internet Use Is Not Private

We reserve the right to monitor employee use of the Internet at any time. You should not expect that your use of the Internet—including but not limited to the sites you visit, the amount of time you spend online, and the communications you have—will be private.
