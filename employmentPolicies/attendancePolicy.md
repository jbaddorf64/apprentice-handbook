## General Attendance

At MAXX Potential we offer a flexible work schedule. Hours may also vary depending on work location and job responsibilities. Advisors will provide employees with their work schedule, after consulting with the apprentice on their desired schedule.

We primarily work with customers who operate during the normal business hours of 8am-6pm Monday through Friday, which are considered core business hours at MAXX Potential. Any schedules that include shifts performed outside of this schedule need to be approved in advance by your advisor. 

Any change in your normal schedule must also be approved by your advisor in advance.

MAXX Potential serves the needs of paying customers, which requires that we are able to accurately manage our capacity for performing work. We do not tolerate absenteeism without notice or reasonable cause. Apprentice employees who will be late to or absent from work should notify their advisor in advance, or as soon as possible in the event of an emergency. Chronic absenteeism may result in disciplinary action, up to and including termination of employment.

Apprentice employees who need to leave early, for illness or otherwise, should inform their advisor before departure. Unauthorized departures may result in disciplinary action up to and including termination of employment. 

## Tardiness

Employees are expected to arrive on time and ready for work. An employee who arrives more than 5 minutes after their scheduled arrival time is considered tardy. MAXX Potential recognizes that situations arise which hinder punctuality; regardless, excessive tardiness is prohibited, and may be subject to disciplinary action up to and including termination of employment. 

## Breaks/Lunch Policy

All employees are entitled to one 30-minute break for every 8 hours worked. This may be taken as a single break, or in multiple, smaller increments. Any time off in excess of this will be unpaid. Please notify your advisor in advance, and be sure to clock out prior to departure.

## Time Clock

MAXX Potential uses a time clock system for recording employee time and calculating payroll. Apprentices should clock in when their scheduled shift begins and clock out before they leave. Falsification of time records or having someone else clock in and out for you is cause for immediate dismissal. Failure to properly clock in and out causes additional payroll administration tasks and may result in a delay in receiving your paycheck.