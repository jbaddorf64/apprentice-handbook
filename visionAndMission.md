# Vision and Mission

At MAXX, we envision a world where we are always learning, love what we do, and actively elevate our communities and organizations by MAXXimizing our personal contributions.

We do this by cultivating a safe, hands-on learning environment that nourishes curiosity and encourages risk-taking in order to fill highly skilled jobs in alignment with the market's technology workforce needs.

_That's a long way of saying: "We are passionate technologists who never stop learning!"_
