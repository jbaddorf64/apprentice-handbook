# How Do I Request Time Off?


If you want to request time off, there is an option to do so through the When I Work Dashboard. Try to submit these requests at least 2 weeks in advance so that we cover any project requirements. Talk to your advisor or the leadership team if you have any questions about whether the request will be approved.

It would also be a good idea to make sure that your advisor and anyone on the team you are working on knows of your absence before you leave so they can be prepared to manage the workload without you.