# How Do I Level Up?

These documents are meant to provide a guide to the skills necessary before advancing to each level. If you have free time to focus on learning, you can use these to figure out what you should be working on. If you think you meet all of the requirements on these guides and are ready to move to the next level, bring it up with your advisor during your next one on one. This is a valuable time where they can quiz you and see if they feel comfortable putting you up for the interview process. 

If chosen to interview, you can sign up for a time slot during the next interview day. See the [What Will The Interview Be Like?](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/lifeAtMaxx/interview.md?at=master) Page for more information on how the interview process will go. 


* [Level 1](https://docs.google.com/spreadsheets/d/1WAYCjReH04hv3dmqNmG2cbrImVYKiS7dD06DosP21_o/edit#gid=0)
* [Level 2](https://docs.google.com/spreadsheets/d/1CyBJnkoV7zrFSfSVyj7SlBEtFBSw5HqT_hd_GTduNUs/edit#gid=0)
* Level 3 - Still being developed, as by this point the goal is to have more focused requirements based on your development path. 

*Note: The Titles are based on the level you are currently on, not the level you are aiming for in a promotion*