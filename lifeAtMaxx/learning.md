# Where Should I Learn This Stuff?

At MAXX, there are many different sources of information to help aide you in learning new material. This is a list that will be constantly growing as new resources are found and added. If the site does not provide a free Login, ask your MAXX Buddy and they should be able to provide you with the MAXX Login Credentials.

* Lynda.com
* teamtreehouse.com
* codecademy.com
* codeschool.com
* reddit.com/r/dailyprogrammer/wiki/challenges
* ryanstutorials.net/linuxtutorial/commandline.php
* cram.com/flashcards/computer-programming-vocabulary-1-3035629
* premium.wpmudev.org/blog/35-resources-for-kick-ass-wordpress-developers/

If you cannot find the resources you need from these sites, then as always, your MAXX buddy will be happy to help.