# What Should I Wear?

## Everyday Attendance

Here at MAXX we have a very laid back dress code. We trust you to use your best judgement and dress comfortably and appropriately.

## Client Interaction

While representing MAXX Potential in direct interactions with our customers, we ask that you follow a business casual dress code. We always want to be perceived as prepared and professional.