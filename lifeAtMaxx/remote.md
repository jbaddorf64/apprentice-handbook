# Can I Work From Home?

As an employee of MAXX Potential, the primary goal should be preparing yourself for a career in technology. Learning to work in an office setting and how to interact with co-workers is an important part of that learning process. You should expect to work from the office to get the most out of this experience.

If you have a specific illness that prevents this or might need other support, you should talk with the leadership team about your needs for remote work, flexible time, etc.