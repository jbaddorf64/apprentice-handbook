# When Should I Talk to Who?

## When to Google

In your first days, you will probably face many challenges throughout your work on various projects. If you come accross a problem, one of the best ways to learn is by finding out if any one else faced the same issue before. Chances are a google search will provide many helpful resources of people overcoming the same issues you are facing. Stackoverflow is a great source maintained by developers for developers. Make sure that if you are not finding anything relating to your issue that you try rephrasing your search. Check the console logs and search for specific error codes.

## When to talk to your MAXX Buddy

Your MAXX Buddy is meant to be a valuable resource to you. They will help you get set up on your first day, they will help you figure out valuable learning tracks during your first week, and they will be an easy go to person throughout your first six months. 

If you come across any issues and have exhausted all of the efforts you can think of from above, then reach out to your buddy. Chances are they may have faced the same issue at some point in their development experiences and can point you to a good resource or guide you through the problem. If they have not faced a similar issue, maybe they know someone else on the MAXX Team who has, and can point you to that person. We are a team environment, and sometimes the best learning comes from each other. 

## When to talk to your Advisor

You should make sure you get set up with one-on-ones with your advisor. This one-on-one time is extremely valuable, and can help coach you through any subject matter you want. If there is some concept you are having a hard time grasping, bring it up during a one-on-one and your advisor can help you build a mental model around that concept. If you are on a very specific project and feel that you are missing out on some of the computer science concepts needed for the next level promotion, your advisor can help coach you on those materials.

Before approaching your advisor with an issue, make sure to ask yourself: Did I research every possibility for my problem online? Did I talk to my MAXX Buddy, or any of my teammates to see if maybe they could help me with the issue I am facing? 

## When you have an idea or comment about MAXX

If you have any suggestions, comments, concerns, or ideas about MAXX, you can send it to the [online response form](http://feedback.MAXXpotential.com). This is a resource available to you, and you can provide the feedback annonymously if you wish. The email box is optional, but if you would like a direct response then go ahead and leave your email with your comment.

If you would prefer, also feel free to talk directly to any of the leadership team, your advisor, or your MAXX buddy.

## When you want to report any issues

See [What If I Need Anything Else?](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/lifeAtMaxx/openDoor.md?at=master)