# Who's Who

## Kim Mahan (CEO, Master Technologist)
kim@maxxpotential.com

## John Dlugokecki (COO, Master Technologist)
john@maxxpotential.com

## Elizabeth Papile (Designer)
elizabeth@maxxpotential.com

## Cassie Hohensee (Executive Assistant)
cassandra@maxxpotential.com

## Jorge Vargas (Technical Advisor)
jorge@maxxpotential.com

## Trish Mahan (Technical Advisor)
trish@maxxpotential.com

## Gonzo (Communications Liaison)
gonzo@maxxpotential.com

## Students
* Aaron Throckmorton
* Austin McCracken
* Brian Simoni
* Han Ha
* Nick Bellucci
* Rhey Igou


## Apprentices
Level 1           | Level 2          | Level 3        | Level 4
:----------------:|:----------------:|:--------------:|:--------------:
Charles Loschiavo | Bill Fitzgerald  | Brandon Bihr   | Patrick Sutton
Charles Nelson    | Elijah Wilkes    | Rachel Dorn    | Steven Hubbard
Chris Coates      | James Hounshell  | David Gibson   |
David Spencer     | Justin Baddorf   |                |
Doug Hocker       | Ken Harbin       |                |
Greg Farley       | Lance Goff       |                |
Jaymz Mahan       | Phil Sheppard    |                |
Joe Mallory       |                  |                |
Kari Fox          |                  |                |
Ryan Beyerlein    |                  |                |
Ryan Jacques      |                  |                |
Taylor Jones      |                  |                |
                  |                  |                |
