# What Will the Interview be Like?

The interviews for leveling up are designed to not only make sure you are ready, but also to prepare you for real job interviews. We are all on your side and genuinely hope you succeed. We ask that you help us help you by coming prepared and taking the interview seriously.

## Preparing for the Interview

1. Talk with your Advisor to go over all of the topics that are covered in the Learing Objectives.
2. Prepare visual examples of your work to present at your interview. A web-based portfolio is highly recommended, and feel free to be creative. 
3. Dress appropriately. These interviews are meant to prepare you for real interviews at real companies so you should dress as such.


## Format of an Interview

1. Present your work - 10m
2. Questions about your work - 10m
3. Demonstrate your skills - 15m
4. Questions about your knowledge - 5m
5. Deliberation - 10m
6. Feedback and results - 10m

For More information on preparing for the Interview, there is a Tab on each of the Level Objective sheets.