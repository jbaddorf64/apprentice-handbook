# How Should I Act

At MAXX, we like to keep a friendly, community atmosphere that encourages people to learn and grow into strong future employees in the technology field. 

To keep this environment, we ask that you remember the [MAXX Values](https://bitbucket.org/maxxpotential/apprentice-handbook/src/master/MAXXValues.md?at=master) and respect your fellow coworkers. Do not be afraid of asking questions, and when someone comes to you with a question try to help as best as you can. 

While we expect you to act professionally, we hope that you will feel comfortable learning and solving problems with other apprentices. Healthy discussion is encouraged as long as it's respectful and not loud or disruptive.

Remember that we expect you to be kind and respectful with everyone. No exceptions. Some people may be more easily offended than others, so as always, use your best judgement.