# Health Benefits

Given the educational nature of our business, MAXX Potential does not provide healthcare coverage for Apprentices. Our goal is to provide a safe learning environment where Apprentices can gain knowledge and experience in technology, which we hope will ultimately lead to a long term career in the industry.